# encoding utf-8

## usage: 
# ruby pattern_matching.rb pattern string
##

# Pattern: "abab", input: "redblueredblue" should return true.
# Pattern: "aaaa", input: "asdasdasdasd" should return true.
# Pattern: "aabb", input: "xyzabcxzyabc" should return false.

def search(pattern, string, matches=nil)
    matches = {} if matches.nil?
    puts pattern.inspect
    return true if not pattern and not string

    if pattern
    	head = pattern[0]
    else
        return false
    end

    expect = matches[head]

    if expect
        if string.start_with?(expect)
            return search(pattern[1..pattern.length], string[expect.length..string.length], matches)
        else
            return false
        end
    else
    	if string.length > 0
	    	(1..string.length).to_a.reverse.each do |matchLen|
	            match = string[0..matchLen]
	            #puts matches
	            matches[head] = match
	            if search(pattern[1..pattern.length], string[match.length..string.length], matches)
	                return true
	            else
	                matches.delete head
	            end
	        end
        else
            return false
        end
    end
end

def pattern_test pattern, input
	puts "pattern: #{pattern}, input: #{input}"
	result = search(pattern, input)
	puts "result: #{result}" # if result == true or result == false
end

puts pattern_test("baba", "redblueredblue")
puts pattern_test("aaaa", "asdasdasdasd")
puts pattern_test("aabb", "xyzabcxzyabc")
